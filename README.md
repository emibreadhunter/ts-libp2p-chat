## P2pLib chat (learning purposes)

Simple command line typescript chat - uses libp2p and pubsub communication.

### Docker usage

* Requirements:

docker >= 19

docker-compose >= 1.25

* Build the image and run containers:

`docker-compose build`

`docker-compose up --scale chat=3`

This will start a mesh of 3 peers - all communicating with each other.
 
* To begin sending messages as users attach to the running containers.

`docker attach p2pchat_chat_1`

`docker attach p2pchat_chat_2`

### Local usage

`npm install`

`npm tsc run`

`node dist/index.js`

Passing argument to index.js will set the custom user name:

`node dist/index.js John`
