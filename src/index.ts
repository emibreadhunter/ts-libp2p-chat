
import { Message } from "libp2p-gossipsub/src/message"
import { ChatInterface, Libp2pInterface } from "./interfaces"

const Libp2pModule = require('libp2p')
const TCP = require('libp2p-tcp')
const { NOISE } = require('libp2p-noise')
const MPLEX = require('libp2p-mplex')
const Process = require('process')
const GossipSub = require('libp2p-gossipsub')
const mDNS = require('libp2p-mdns')
const uint8ArrayFromString = require('uint8arrays/from-string')
const uint8ArrayToString = require('uint8arrays/to-string')
const readline = require('readline');


class Chat implements ChatInterface {
	libp2p: Libp2pInterface
	topic: string
	userName: string

	constructor(libp2p: Libp2pInterface, topic: string) {
		this.libp2p = libp2p
		this.topic = topic
		this.userName = ""
		this.handleSignals()
		this.setUserName()
		this.connect()
	}

	handleSignals() {
		process.on('SIGTERM', () => this.leave())
		process.on('SIGINT', () => this.leave())
	}

	async start() {
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
		for await (const line of rl) {
			if (line != "") {
				await this.sendMessage(line)
			}
		}
	}

	async sendMessage(msg: string, delimit: boolean = true) {
		const delimiter: string = delimit ? ":" : ""
		await this.libp2p.pubsub.publish(this.topic, uint8ArrayFromString(`${this.userName}${delimiter} ${msg}`))
	}

	async connect() {
		this.libp2p.pubsub.on(this.topic, (msg: Message) => {console.log(uint8ArrayToString(msg.data))})
		this.libp2p.pubsub.subscribe(this.topic)
		await this.sendMessage("connected", false)
	}

	async leave() {
		await this.sendMessage("left", false)
		this.libp2p.pubsub.removeListener(this.topic, () => {})
		this.libp2p.pubsub.unsubscribe(this.topic)
		await this.libp2p.stop()
		process.exit(0)
	}

	setUserName () {
		const peerId = this.libp2p.peerId.toJSON().id
		let userName = `${peerId.slice(0, 10)}`
		if (Process.argv.length >= 3) {
			userName = Process.argv[2];
		}
		this.userName = userName
	}
}

const main = async () => {
	const topic = 'Chat'
  const options = {
    addresses: {
      listen: ['/ip4/0.0.0.0/tcp/0']
    },
    modules: {
      transport: [TCP],
      connEncryption: [NOISE],
			streamMuxer: [MPLEX],
			peerDiscovery: [mDNS],
      pubsub: GossipSub,
    },
    config: {
			peerDiscovery: {
				autoDial: true,
			},
			pubsub: {
					enabled: true,
					emitSelf: true,
					signMessages: true,
					strictSigning: true
			}
    }
  }

	const libp2p = await Libp2pModule.create(options)
  await libp2p.start()

	let chat = new Chat(libp2p, topic)
	await chat.start()
}

main()
