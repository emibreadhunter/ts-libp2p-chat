import { ConnectionManager, Libp2p } from "libp2p-gossipsub/src/interfaces"

export interface PubsubInterface {
	on(topic: string, handler: Function): void
	subscribe(topic: string): void
    publish(topic: string, data: Uint8Array): Promise
    unsubscribe(topic: string): void
    removeListener(topic: string, handler: Function): void
}

export interface ConnectionManagerInterface extends ConnectionManager {
	on(topic: string, handler: Function): void
}

export interface Libp2pInterface extends Libp2p {
	pubsub: PubsubInterface,
	connectionManager: ConnectionManagerInterface
    stop(): Promise
    isStarted(): boolean
}

export interface ChatInterface {
    libp2p: Libp2pInterface
	topic: string
    userName: string
    start(): Promise
    connect(): Promise
    leave(): Promise
    setUserName(): void
    sendMessage(msg: string, delimit: boolean): Promise
    handleSignals(): void
}
