FROM node:15.4.0-buster

RUN apt-get -y update && apt-get -y upgrade && apt-get install -y net-tools  

RUN node -v
RUN cat /etc/os-release

WORKDIR /app
COPY package*.json ./
RUN npm install

COPY . /app

WORKDIR /app/src
RUN pwd